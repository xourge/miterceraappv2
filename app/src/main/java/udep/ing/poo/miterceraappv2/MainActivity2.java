package udep.ing.poo.miterceraappv2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MainActivity2 extends AppCompatActivity implements AdapterView.OnItemClickListener, TextView.OnEditorActionListener {

    ListView listView;
    EditText editTextTextPersonName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        editTextTextPersonName = (EditText)findViewById(R.id.editTextTextPersonName);
        editTextTextPersonName.setOnEditorActionListener(this);

        SharedPreferences sp = getSharedPreferences("m2",MODE_PRIVATE);
        String nombre = sp.getString("nombre","");
        editTextTextPersonName.setText(nombre);

        items = new ArrayList<String>();
        items.add("Carla");
        items.add("Carlos");
        items.add("Cesar");
        items.add("Erick");
        items.add("Gonzalo");

        Set<String> lista = sp.getStringSet("lista",new HashSet<>(items));
        items = new ArrayList<String>(lista);

        listView = (ListView)findViewById(R.id.listView);
        setListView();
    }


    private void setListView() {
        List<String> items = getItems();
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, items);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);
    }

    List<String> items = new ArrayList<String>();

    private List<String> getItems() {
        String nombre = editTextTextPersonName.getText().toString();
        for(int i=0; i<items.size(); i++) {
            if (!items.get(i).contains(nombre)) {
                items.remove(i);
                i--;
            }
        }
        return items;

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

        if (adapterView.getId() == listView.getId()) {

            String seleccionado = adapterView.getItemAtPosition(i).toString();
            Toast.makeText(getApplicationContext(), seleccionado, Toast.LENGTH_SHORT).show();

            Set<String> set = new HashSet<String>(items);
            SharedPreferences.Editor editor = getSharedPreferences("m2",MODE_PRIVATE).edit();
            editor.putStringSet("lista",set);
            editor.commit();

            Intent intent = new Intent(this,MainActivity.class);
            startActivity(intent);

        }

    }

    @Override
    public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {

        if (textView.getId() == editTextTextPersonName.getId()) {

            String nombre = editTextTextPersonName.getText().toString();
            Toast.makeText(getApplicationContext(),nombre,Toast.LENGTH_SHORT).show();
            setListView();

            SharedPreferences.Editor editor = getSharedPreferences("m2",MODE_PRIVATE).edit();
            editor.putString("nombre",editTextTextPersonName.getText().toString());
            editor.commit();

        }

        return false;
    }
}