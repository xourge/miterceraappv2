package udep.ing.poo.miterceraappv2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private Spinner spinner;
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        spinner = (Spinner)findViewById(R.id.spinner);

        List<String> items = new ArrayList<String>();
        items.add("Alberto");
        items.add("Angel");
        items.add("Caleb");
        items.add("Carla");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, items);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(this);

        textView = (TextView)findViewById(R.id.textView);
        SharedPreferences sp = getSharedPreferences("m2",MODE_PRIVATE);
        String nombre = sp.getString("nombre","");
        textView.setText(nombre);
    }

    public void showToastRapido(View v) {

        Toast.makeText(getApplicationContext(), "toast rápido", Toast.LENGTH_SHORT).show();

    }

    public void showToastLento(View v) {

        Toast.makeText(getApplicationContext(), "toast lento", Toast.LENGTH_LONG).show();

    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

        if (adapterView.getId() == spinner.getId()) {

            String seleccionado = adapterView.getItemAtPosition(i).toString();
            Toast.makeText(getApplicationContext(), seleccionado, Toast.LENGTH_LONG).show();

        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}